<div align="middle">

# Table of Content


[Week 1](#week1)

[Week 2](#week2)

[Week 3](#week3)

[Week 4](#week4)

[Week 5](#week5)

[Week 6](#week6)

[Week 7](#week7)

[Week 8](#week8)

[Week 8](#week8)

[Week 9](#week9)

[Week 10](#week10)

[Week 11](#week11)

[Week 12](#week12)

[Week 13](#week13)

[Week 14](#week14)

</div>



## Week 1

- Agenda and Goals
- Problem and solution

| Problem | Solution |
|:---:|:---:|
|Dont understand jobscope| Ask for member opinion on the control system|

- Impact


__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Share idea| Learn Markdown|
||Create gitlab introduction|


## Week2

_Agenda and Goals_

1. Learn simulink to have proper skillset for control system

2. Create a Gantt Chart for future reference.

3. Help others in moving forward in their respective subsystem 

_Problem and solution_

| Problem | Solution |
|:---:|:---:|
|Don't know what to prepare for control system|Ask azizi for guidance|
|Not sure about out timeline|Discussing about our Gantt Chart using azizi sources as reference|
|Don't know how to use simulink|Ask group member to teach simulink|

_Impact_

1. My subsystem have a guideline to follow in suggesting and dividing work among us.
2. Our Gantt chart is more relatable to our tasks timeline.
3. My understanding in simulink have improved for future tasks.

__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Learn simulink|Help zaim in flowchart|
|Adjust Gantt Chart| |
|Help ask azizi for guidance||

## Week3

_Agenda and Goals_

1. Futher understand the previous research about control system

_Problem and solution_

| Problem | Solution |
|:---:|:---:|
|Require more information from previous research paper| Arrange a meeting with azizi|

_Impact_

1. Arranged meeting hopefully can answer our question regarding the previous study.
2. Improved understanding about control system from previous research paper.

__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Arrange meeting with azizi|Help classmates in understanding gitlab|
|Understanding previous research paper|  |

## Week4

Agenda and Goals_

1. Futher understand the previous research about control system


_Problem and solution_

| Problem | Solution |
|:---:|:---:|
|Require more information from previous research paper| Meet azizi at lab H2.1|


_Impact_

1.Improved understanding about we need to do for control system
2.Understand importance of Mission Planner in this project

__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Meet with azizi|Easy|
|Try Mission Planner|  |

## Week5

_Agenda and Goals_

1. Understand how to calibrate flight controller using Mission Planner
2. Try to create a meeting where everyone can participate


_Problem and solution_

| Problem | Solution |
|:---:|:---:|
|Group doesn't understand current progress| Create meeting at lab|


_Impact_

1. Each person understand our current progress in mission planner
2. More people come to meeting

__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Meet with everyone|Check everyone progress on IDP|
|Do calibration on drone||

## Week6

Agenda and Goals_

1. Finished subsystem gitlab for control system
2. Achieved completed firmware to be implemented into airship.
3. Understand firmware that we will be using during project.


_Problem and solution_

| Problem | Solution |
|:---:|:---:|
|Need more information for firmware| Ask Fikriand Azizi|
|Subsystem gitlab is quite a alot of work| Ask others help to complete the gitlab|



_Impact_

1. Started to understand about firmware that we are going to use in the project

__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Ask Azizi and Fikri about firmware|Help other subsystem in lab|
|Continue observing subsystem Gitlab|Understand about other subsystem progress|

## Week7

- Agenda and Goals

 understanding about what to do with the firmware

- Problem and solution

| Problem | Solution |
|:---:|:---:|
|Lack of knowledge about what to do| Ask Azizi and Fikri|

- Impact

Knowledge about required data from Mission Planner is acquired


__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Learn about data|Help other subsystem|


## Week8

- Agenda and Goals
- Problem and solution

| Problem | Solution |
|:---:|:---:|
|Lack of knowledge about what to do| Ask Azizi and Fikri|

- Impact


__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Share idea| Learn Markdown|
||Create gitlab introduction|

## Week9

- Agenda and Goals
- Problem and solution

| Problem | Solution |
|:---:|:---:|
|Lack of knowledge about what to do| Ask Azizi and Fikri|

- Impact


__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Share idea| Learn Markdown|
||Create gitlab introduction|

## Week10

- Agenda and Goals
- Problem and solution

| Problem | Solution |
|:---:|:---:|
|Dont understand jobscope| Ask for member opinion on the control system|

- Impact


__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Share idea| Learn Markdown|
||Create gitlab introduction|

## Week11

- Agenda and Goals
1. Help solve sprayer to turn on/off
2. Uploading firmware into flight controller
- Problem and solution

| Problem | Solution |
|:---:|:---:|
|Don't know what kind of mechanism to used| Someone from group experienced in the sprayer mechanism|
|Firmware cannot be uploaded into mission planner|Asking azizi about his opinion|

- Impact
1. Sprayer mechanism is mostly completed.
2. Firmware will be tested to be uploaded using other method next week
3. Azizi will try to come next tuesday


__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Help in sprayer mechanism|Help sprayer team in testing|
|Try uploading firmware||
|Ask azizi about meeting||

## Week12

- Agenda and Goals
1. Upload Firmware
2. Test the component with the firmware
- Problem and solution

| Problem | Solution |
|:---:|:---:|
|Upload firmware| Azizi come lab to help|
|Servo cannot be powered up|Buy UBEC to help power up the servo|

- Impact
1. UBEC will arrived about a few days
2. The control system will be tested once the UBEC have arrived


__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Arrange meet with azizi| Check others gitlab|
|Find solution for ESC||

## Week13

- Agenda and Goals
1. Complete Firmware Airship
2. Test UBEC
3. Check Motor and Servo setting
4. Create Checklist(Before,Pre,Post)

- Problem and solution

| Problem | Solution |
|:---:|:---:|
|UBECs do not provide signal wire | Ask FSI(Haikal) and fikri to help|
|Wire from servo is short to connect to thruster arm| FSI(Haikal,Zharif,Chewan) help|

- Impact
1. Motor and servo can move
2. Servo can connect to airship

__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Connect UBEC to flight controller|Help in check wiring connection with FSI(Haikal)|
|Configure max/min for servo||
|Create checklist for subsystem||

## Week14

- Agenda and Goals
1. Complete Firmware airship
2. Try Motor and Servo with airship
3. Test Flight
- Problem and solution

| Problem | Solution |
|:---:|:---:|
|New firmware cannot move servo with motor simultaneously| Ask azizi to change the firmware code|
|UBECs is hanging outside gondola|UBECs is double taped outside gondola|
|Some servo have problem to move|Set the servo max/min to 900/2000 from 500/2400|
|Changing firmware cause the flight controller to change the configuration|Continuous calibration is done|

- Impact
1. Motor and servo can move simultaneously
2. UBECs is safer
3. Servo movement help to improve safety and better accuracy

__Summary Task__

| Subsystem | Group |
|:---:|:---:|
|Communicating with azizi|Help in wiring|
|Calibrating servo||
|Continuously calibrating the flight controller||


[Back to Fortnite](#table-of-content)
</div>

